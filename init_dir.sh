#creating directories for mounting on database containers
MASTER_DATA_DIR=./data/master-db
rm -rf $MASTER_DATA_DIR
mkdir -p $MASTER_DATA_DIR
chown 26:26 $MASTER_DATA_DIR
chcon -Rt svirt_sandbox_file_t $MASTER_DATA_DIR

REPLICA_DATA_DIR=./data/replica-db
rm -rf $REPLICA_DATA_DIR
mkdir -p $REPLICA_DATA_DIR
chown 26:26 $REPLICA_DATA_DIR
chcon -Rt svirt_sandbox_file_t $REPLICA_DATA_DIR

