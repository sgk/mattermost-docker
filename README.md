### How to run Mattermost with crunchy postgresql cluster on Docker

First create the directories for Persistent storage

```
./init_dir.sh
```

then do;
```
docker-compose up
```

it will take a while to complete the deployment,


After the deployment got complete 
go to localhost:8088 on browser